export default [
  {
    name: 'firstTab',
    label: 'First Tab',
    content: '<h2>Mauris dignissim imperdiet purus</h2><p>Sed luctus nisl vestibulum nec. Quisque tempor scelerisque augue quis consectetur. Aenean tincidunt quis justo a dignissim. Donec varius luctus placerat. Nullam fringilla, dui a suscipit fringilla, purus sem rutrum nibh, at <strong>malesuada purus mauris eu tortor</strong>. Curabitur eu arcu sapien. Morbi suscipit a dui eu dictum. Sed gravida volutpat sem non pharetra. Fusce ultricies nisl tellus, sed fermentum lorem pellentesque in. Aliquam sit amet dui vel tellus lobortis tincidunt.</p><p>Duis sapien mauris, semper ut varius ullamcorper, consectetur et nisl. Nulla nec maximus nisi. Mauris vel felis eu felis tincidunt tempus et non lorem. Nunc vel arcu sagittis, sagittis ex quis, dictum lectus. Maecenas condimentum est vel metus sagittis, in suscipit est dictum. Morbi eleifend magna tortor, in porta nisi accumsan ut. Fusce pretium dui nec tincidunt sagittis.</p>'
  },
  {
    name: 'secondTab',
    label: 'Second Tab',
    content: '<ul><li>Donec pretium</li><li>Augue non erat laoreet</li><li>Dolor non semper lacinia</li><li>Tortor diam malesuada</li></ul>Nunc auctor, nisi non interdum dapibus, nisl magna finibus magna, eget facilisis dui diam ut ipsum. Pellentesque mi metus, pretium et est tempus, blandit aliquam felis. Aenean vel elit congue, finibus arcu in, condimentum orci. Sed pretium nec metus et semper. Maecenas ullamcorper nunc arcu, id blandit tellus iaculis ut.'
  }
]